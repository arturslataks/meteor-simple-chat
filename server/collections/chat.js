/**
 * Chat collection server methods.
 * 
 * @author Scandiweb <info@scandiweb.com>
 * @copyright Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

Meteor.methods({
    /**
     * Add chat message
     */
    chatAddMessage : function (message) {
        if (this.userId) {
            Chat.addMessage(message);
        }
    }
});

Chat.addMessage = function (message) {
    if (!message || message == '') {
        return;
    }

    this.insert({
      user_id    : Meteor.userId(),
      message    : message,
      created_at : new Date()
    })
};