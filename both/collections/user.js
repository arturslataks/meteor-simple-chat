/**
 * File that defines User collection functions and data.
 * 
 * @author Scandiweb <info@scandiweb.com>
 * @copyright Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

if (Meteor.isServer) {
    Meteor.publish('userData', function () {
        /* Publish chat data only for logged in user */
    	if (!this.userId) {
			return [];
    	}

        return Meteor.users.find({}, {
            fields: {
                'profile.name'        : 1,
                'services.facebook.id': 1
            }
        });
    });
}

/* Subscribe to user data right away */
if (Meteor.isClient) {
    Meteor.subscribe('userData');
}