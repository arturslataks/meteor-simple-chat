/**
 * File that defines Chat collection functions and data.
 * 
 * @author Scandiweb <info@scandiweb.com>
 * @copyright Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

/* Define new collection */
Chat = new Mongo.Collection('chat');
// Chat.watcher = false;

if (Meteor.isServer) {
    Meteor.publish('chatData', function () {
        /* Publish chat data only for logged in user */
        if (!this.userId) {
            return [];
        }

        return Chat.find({}, {
            /* Fields returned from query */
            fields: {
                'message'   : 1,
                'user_id'   : 1,
                'created_at': 1
            },
            sort: {created_at : -1}
        });
    });
}

/* Subscribe to the collection right away on client */
if (Meteor.isClient) {
    Meteor.subscribe('chatData');
}

/**
 * Getting all messages from chat.
 */
Chat.getMessages = function () {
    var messages = this.find({}, {sort: {created_at : 1}});

    /* Think of something that will not trigger sound on initial page load */
    // if (!Chat.watcher) {
    //     Chat.watcher = messages.observeChanges({
    //         added: function (id, message) {
    //             console.log(message.user_id, Meteor.user()._id);
    //             if (message.user_id === Meteor.user()._id) {
    //                 var audio = new Audio('/notify.mp3');
    //                 audio.sound = 0.2;
    //                 audio.play();
    //             }
    //         }
    //     })
    // }

    /* Doing this sort of join, but simply mapping data */
    mapped = messages.map(function (part, index, array) {
        part.user = Meteor.users.findOne({_id: part.user_id});
        return part;
    });

    return mapped
};