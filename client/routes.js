/**
 * Deining application routes.
 *
 * @author Scandiweb <info@scandiweb.com>
 * @copyright Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

Router.map(function () {
    this.route('chat', {
        path       : '/',
        template   : 'chat'
    });
});