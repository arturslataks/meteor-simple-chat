/**
 * Define chat controller. Only controller for this application.
 * 
 * @author Scandiweb <info@scandiweb.com>
 * @copyright Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

ChatController = RouteController.extend({

    /* Waiting on specific subscriptions */
    waitOn: function () {
        return Meteor.subscribe('userData') && Meteor.subscribe('chatData');
    },

    /* Data that will be passed along with route */
    data: function () {
        return { messages: Chat.getMessages() }
    }
});