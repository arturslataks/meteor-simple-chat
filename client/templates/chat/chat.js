/**
 * Chat view template events and helpers
 *
 * @author Scandiweb <info@scandiweb.com>
 * @copyright Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

Template.chat.helpers({
    /**
     * Function changes text into emoticons
     */
    parseMessage: function () {
        var text = this.message;

        /* Escape special characters used emoticon codes/aliases */
        var specialRegex = new RegExp('(\\)|\\(|\\*|\\[|\\]|\\{|\\}|\\||\\^|\\<|\\>|\\\\|\\?|\\+|\\=|\\.|\\$)', 'g'),
            preMatch = '(^|[\\s\\0])';

        _.each(Global.Emoticons, function (emoticon) {
            var emoticonClass = 'emoticon';
            _.each(emoticon.replacements, function (replacement) {
                /* Check if text contains only single icon, add class to make it large */
                if (replacement === text) {
                    emoticonClass += ' emoticon-large';
                }
                var replaceStr = replacement.replace(specialRegex, '\\$1'),
                    match = new RegExp(preMatch + '(' + replaceStr + ')', 'g');
                text = text.replace(match, ' <img class="' + emoticonClass + '" src="' + emoticon.image + '" alt="' + replacement + '" />');
            });
        });

        return text;
    },

    /**
     * Checking if message sender is current user
     */
    isAuthor: function () {
        return this.user._id === Meteor.user()._id ? 'is-author' : '';
    }
});

Template.chat.events({
    /* Login user with facebook when button is clicked */
    'click .fb-login': function () {
        var permissions = [];
        Meteor.loginWithFacebook({requestPermissions: permissions}, function (error) {
            if (error) {
                alert('Unable to login with Facebook');
                return;
            }
            alert('Welcome to chat app!');
        })
    },
    
    /**
     * Send message
     */
    'click button.send-message': function (e, template) {
        if (Meteor.user()) {
            var textarea = template.$('textarea.send-message');

            Meteor.call('chatAddMessage', textarea.val(), function (error, result) {
                if (error) {
                    alert('Message was not sent :|');
                } else {
                    textarea.val('');
                }
            });
        }
    },

    /**
     * Send message when hitting enter.
     */
    'keyup textarea.send-message': function (e, template) {
        if (Meteor.user() && e.keyCode === 13) {
            var textarea = template.$('textarea.send-message');

            Meteor.call('chatAddMessage', textarea.val(), function (error, result) {
                if (error) {
                    alert('Message was not sent :|');
                } else {
                    textarea.val('');
                }
            });
        }
    }
});