/**
 * Global functions namespace
 */
Global = {};

/**
 * Get the CDN url for emoticons
 */
Global.getCDNUrl = function (path) {
	return 'http://d3t31op3zrvtg.cloudfront.net/static' + path;
};