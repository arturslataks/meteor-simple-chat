Global.Emoticons = [
    {
        image: Global.getCDNUrl('/images/emoticons/angel.svg'),
        replacements: ['(angel)']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/angry.svg'),
        replacements: ['(angry)', ':[']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/confused.svg'),
        replacements: [':\\', ':-\\', '-_-']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/cool.svg'),
        replacements: ['8)', '8-)']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/crying.svg'),
        replacements: [';(', ';-(']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/depression.svg'),
        replacements: [':(', ':-(']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/dislike.svg'),
        replacements: ['(n)']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/humor.svg'),
        replacements: ['x-D']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/kiss.svg'),
        replacements: [':*']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/laughing.svg'),
        replacements: [':D', ':-D']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/like.svg'),
        replacements: ['(y)']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/love.svg'),
        replacements: ['(h)']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/money.svg'),
        replacements: ['$_$']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/mustache.svg'),
        replacements: [':}']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/robot.svg'),
        replacements: [':]', ':-]']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/security.svg'),
        replacements: [':X', ':-X']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/sleepi.svg'),
        replacements: ['(z)']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/smile.svg'),
        replacements: [':)', ':-)', '=)', '=-)']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/star.svg'),
        replacements: ['*_*']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/sunglass.svg'),
        replacements: ['8D', '8-D']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/surprise.svg'),
        replacements: [':O', ':-O', ':o', 'o_o', 'O_O']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/tongue.svg'),
        replacements: [':P', ':-P']
    },
    {
        image: Global.getCDNUrl('/images/emoticons/wink.svg'),
        replacements: [';)', ';-)']
    }
];